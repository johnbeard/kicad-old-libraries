PCBNEW-LibModule-V1  Fr 12 Jul 2013 20:09:55 CEST
# encoding utf-8
Units mm
$INDEX
OldSowjetaera_Transistor_Type-II_BigPads_RevA
OldSowjetaera_Transistor_Type-II_SmallPads_RevA
OldSowjetaera_Transistor_Type-I_BigPads_RevA
OldSowjetaera_Transistor_Type-I_SmallPads_RevA
$EndINDEX
$MODULE OldSowjetaera_Transistor_Type-II_BigPads_RevA
Po 0 0 0 15 51E0448A 00000000 ~~
Li OldSowjetaera_Transistor_Type-II_BigPads_RevA
Sc 0
AR 
Op 0 0 0
T0 0.1 -8.1 1.524 1.524 0 0.3048 N V 21 N "T"
T1 0.1 7.6 1.524 1.524 0 0.3048 N I 21 N "OldSowjetaera_Transistor_Type-II_BigPads_RevA"
DC -4.8 0 -4.8 0.4 0.381 21
DC 0 0 4 0 0.381 21
DC 0 0 5.6 0 0.381 21
$PAD
Sh "2" O 1.4 2.5 0 0 0
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 -2
$EndPAD
$PAD
Sh "1" O 1.4 2.5 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2 0
$EndPAD
$PAD
Sh "3" O 1.4 2.5 0 0 900
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2 0
$EndPAD
$SHAPE3D
Na "OldSowjetAera_Transistor_Type-II_Faktor03937_RevA.wrl"
Sc 0.3937 0.3937 0.3937
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE OldSowjetaera_Transistor_Type-II_BigPads_RevA
$MODULE OldSowjetaera_Transistor_Type-II_SmallPads_RevA
Po 0 0 0 15 51E04407 00000000 ~~
Li OldSowjetaera_Transistor_Type-II_SmallPads_RevA
Sc 0
AR 
Op 0 0 0
T0 0 -7.1 1.524 1.524 0 0.3048 N V 21 N "T"
T1 0.1 7.6 1.524 1.524 0 0.3048 N I 21 N "OldSowjetaera_Transistor_Type-II_SmallPads_RevA"
DC -4.8 0 -4.8 0.4 0.381 21
DC 0 0 4 0 0.381 21
DC 0 0 5.6 0 0.381 21
$PAD
Sh "2" C 1.4 1.4 0 0 0
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 -2
$EndPAD
$PAD
Sh "1" C 1.4 1.4 0 0 0
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2 0
$EndPAD
$PAD
Sh "3" C 1.4 1.4 0 0 0
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2 0
$EndPAD
$SHAPE3D
Na "OldSowjetAera_Transistor_Type-II_Faktor03937_RevA.wrl"
Sc 0.3937 0.3937 0.3937
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE OldSowjetaera_Transistor_Type-II_SmallPads_RevA
$MODULE OldSowjetaera_Transistor_Type-I_BigPads_RevA
Po 0 0 0 15 51A486FF 00000000 ~~
Li OldSowjetaera_Transistor_Type-I_BigPads_RevA
Sc 0
AR 
Op 0 0 0
T0 0.1 -8.1 1.524 1.524 0 0.3048 N V 21 N "T"
T1 0.1 7.6 1.524 1.524 0 0.3048 N I 21 N "OldSowjetaera_Transistor_Type-I_BigPads_RevA"
DC -4.8 0 -4.8 0.4 0.381 21
DC 0 0 4 0 0.381 21
DC 0 0 5.6 0 0.381 21
$PAD
Sh "2" O 1.4 2.5 0 0 0
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "1" O 1.4 2.5 0 0 0
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2 0
$EndPAD
$PAD
Sh "3" O 1.4 2.5 0 0 0
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2 0
$EndPAD
$SHAPE3D
Na "OldSowjetAera_Transistor_Type-I_Faktor03937_RevA.wrl"
Sc 0.3937 0.3937 0.3937
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE OldSowjetaera_Transistor_Type-I_BigPads_RevA
$MODULE OldSowjetaera_Transistor_Type-I_SmallPads_RevA
Po 0 0 0 15 51A48691 00000000 ~~
Li OldSowjetaera_Transistor_Type-I_SmallPads_RevA
Sc 0
AR 
Op 0 0 0
T0 0 -7.1 1.524 1.524 0 0.3048 N V 21 N "T"
T1 0.1 7.6 1.524 1.524 0 0.3048 N I 21 N "OldSowjetaera_Transistor_Type-I_SmallPads_RevA"
DC -4.8 0 -4.8 0.4 0.381 21
DC 0 0 4 0 0.381 21
DC 0 0 5.6 0 0.381 21
$PAD
Sh "2" C 1.4 1.4 0 0 0
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "1" C 1.4 1.4 0 0 0
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2 0
$EndPAD
$PAD
Sh "3" C 1.4 1.4 0 0 0
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2 0
$EndPAD
$SHAPE3D
Na "OldSowjetAera_Transistor_Type-I_Faktor03937_RevA.wrl"
Sc 0.3937 0.3937 0.3937
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE OldSowjetaera_Transistor_Type-I_SmallPads_RevA
$EndLIBRARY
