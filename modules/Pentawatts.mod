PCBNEW-LibModule-V1  Do 06 Jun 2013 13:43:49 CEST
# encoding utf-8
Units mm
$INDEX
Pentawatt_Neutral_Staggered-Typ1_Horizontal_TO220-5-TA05B
Pentawatt_Neutral_Staggered-Typ2_Horizontal_TO220-5-T05E
Pentawatt_Neutral_Staggered_Verical_TO220-5-T05D
Pentawatt_Neutral_Straight_Vertical_TO220-5-T05A
$EndINDEX
$MODULE Pentawatt_Neutral_Staggered-Typ1_Horizontal_TO220-5-TA05B
Po 0 0 0 15 51B075D4 00000000 ~~
Li Pentawatt_Neutral_Staggered-Typ1_Horizontal_TO220-5-TA05B
Cd Pentawatt, Neutral, Staggered Typ 1, Horizontal, TO220-5, TA05B,
Kw Pentawatt, Neutral, Staggered Typ 1, Horizontal, TO220-5, TA05B,
Sc 0
AR 
Op 0 0 0
T0 0.254 -9.144 1.524 1.524 0 0.3048 N V 21 N "IC"
T1 2.54 5.08 1.524 1.524 0 0.3048 N V 21 N "Pentawatt_Neutral_Staggered-Typ1_Horizontal_TO220-5-TA05B"
T2 -5.588 -0.762 1.524 1.524 0 0.3048 N V 21 N "1"
DS 0 -4.572 0 -0.508 0.381 21
DS -3.556 -4.826 -3.556 -0.508 0.381 21
DS 3.556 -4.826 3.556 -0.762 0.381 21
DS 1.778 -4.572 1.778 -4.064 0.381 21
DS -1.778 -4.572 -1.778 -4.064 0.381 21
DS -5.30098 -4.699 -5.30098 -12.30122 0.381 21
DS 5.30098 -12.19962 5.30098 -4.699 0.381 21
DS -5.30098 -4.699 5.30098 -4.699 0.381 21
DC 0 -16.764 1.778 -14.986 0.381 21
DS 5.334 -12.192 5.334 -20.193 0.381 21
DS 5.334 -20.193 -5.334 -20.193 0.381 21
DS -5.334 -20.193 -5.334 -12.192 0.381 21
DS 5.334 -12.192 -5.334 -12.192 0.381 21
$PAD
Sh "3" O 2.49936 1.80086 0 0 900
Dr 1.19888 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 1.39954
$EndPAD
$PAD
Sh "1" O 2.49936 1.80086 0 0 900
Dr 1.19888 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3.40106 1.39954
$EndPAD
$PAD
Sh "5" O 2.49936 1.80086 0 0 900
Dr 1.19888 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3.40106 1.39954
$EndPAD
$PAD
Sh "" C 3.79984 3.79984 0 0 900
Dr 3.79984 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 0 -16.764
$EndPAD
$PAD
Sh "2" O 2.49936 1.80086 0 0 900
Dr 1.19888 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1.69926 -2.10058
$EndPAD
$PAD
Sh "4" O 2.49936 1.80086 0 0 900
Dr 1.19888 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1.69926 -2.10058
$EndPAD
$SHAPE3D
Na "TO220-5_Pentawatt_Wings3d_RevA_24Oct2012/TO220-5_TO5B_Pentawatt_Horizontal_Staggered_RevA_Faktor03937_24Oct2012.wrl"
Sc 0.3937 0.3937 0.3937
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE Pentawatt_Neutral_Staggered-Typ1_Horizontal_TO220-5-TA05B
$MODULE Pentawatt_Neutral_Staggered-Typ2_Horizontal_TO220-5-T05E
Po 0 0 0 15 51B075F1 00000000 ~~
Li Pentawatt_Neutral_Staggered-Typ2_Horizontal_TO220-5-T05E
Cd Pentawatt, Neutral, Staggered Typ 2, Horizontal, TO220-5, T05E,
Kw Pentawatt, Neutral, Staggered Typ 2, Horizontal, TO220-5, T05E,
Sc 0
AR Pentawatt_Neutral_Staggered-Typ2_Horizontal_TO220-5-T05E
Op 0 0 0
T0 0.254 -9.144 1.524 1.524 0 0.3048 N V 21 N "IC"
T1 2.54 5.08 1.524 1.524 0 0.3048 N V 21 N "Pentawatt_Neutral_Staggered-Typ2_Horizontal_TO220-5-T05E"
DS 1.69926 -0.29972 1.69926 -4.699 0.381 21
DS -1.69926 -0.29972 -1.69926 -4.699 0.381 21
DS 0 -3.79984 0 -4.699 0.381 21
DS 3.50012 -3.79984 3.50012 -4.699 0.381 21
DS -3.40106 -3.79984 -3.40106 -4.699 0.381 21
T2 -5.588 -0.762 1.524 1.524 0 0.3048 N V 21 N "1"
DS -5.30098 -4.699 -5.30098 -12.30122 0.381 21
DS 5.30098 -12.19962 5.30098 -4.699 0.381 21
DS -5.30098 -4.699 5.30098 -4.699 0.381 21
DC 0 -16.764 1.778 -14.986 0.381 21
DS 5.334 -12.192 5.334 -20.193 0.381 21
DS 5.334 -20.193 -5.334 -20.193 0.381 21
DS -5.334 -20.193 -5.334 -12.192 0.381 21
DS 5.334 -12.192 -5.334 -12.192 0.381 21
$PAD
Sh "3" O 2.49936 1.80086 0 0 900
Dr 1.19888 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 -2.10058
$EndPAD
$PAD
Sh "1" O 2.49936 1.80086 0 0 900
Dr 1.19888 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3.40106 -2.10058
$EndPAD
$PAD
Sh "5" O 2.49936 1.80086 0 0 900
Dr 1.19888 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3.40106 -2.10058
$EndPAD
$PAD
Sh "" C 3.79984 3.79984 0 0 900
Dr 3.79984 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 0 -16.764
$EndPAD
$PAD
Sh "2" O 2.49936 1.80086 0 0 900
Dr 1.19888 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1.69926 1.39954
$EndPAD
$PAD
Sh "4" O 2.49936 1.80086 0 0 900
Dr 1.19888 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1.69926 1.39954
$EndPAD
$SHAPE3D
Na "TO220-5_Pentawatt_Wings3d_RevA_24Oct2012/TO220-5_TO5E_Pentawatt_Horizontal_Staggered_RevA_Faktor03937_24Oct2012.wrl"
Sc 0.3937 0.3937 0.3937
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE Pentawatt_Neutral_Staggered-Typ2_Horizontal_TO220-5-T05E
$MODULE Pentawatt_Neutral_Staggered_Verical_TO220-5-T05D
Po 0 0 0 15 4DBC26D0 00000000 ~~
Li Pentawatt_Neutral_Staggered_Verical_TO220-5-T05D
Cd Pentawatt, Neutral, Staggered, Verical, TO220-5, T05D,
Kw Pentawatt, Neutral, Staggered, Verical, TO220-5, T05D,
Sc 0
AR Pentawatt_Vertical
Op 0 0 0
T0 0 -5.08 1.524 1.524 0 0.3048 N V 21 N "IC"
T1 1.27 7.62 1.524 1.524 0 0.3048 N V 21 N "Pentawatt_Neutral_Staggered_Verical_TO220-5-T05D"
T2 -6.35 3.81 1.524 1.524 0 0.3048 N V 21 N "1"
DS 3.302 1.778 5.334 1.778 0.381 21
DS -0.508 1.778 0.508 1.778 0.381 21
DS -5.334 1.778 -3.429 1.778 0.381 21
DS -1.524 -3.048 -1.524 -1.905 0.381 21
DS 1.524 -3.048 1.524 -1.905 0.381 21
DS 5.334 -1.905 5.334 1.778 0.381 21
DS -5.334 1.778 -5.334 -1.905 0.381 21
DS 5.334 -3.048 5.334 -1.905 0.381 21
DS 5.334 -1.905 -5.334 -1.905 0.381 21
DS -5.334 -1.905 -5.334 -3.048 0.381 21
DS 0 -3.048 -5.334 -3.048 0.381 21
DS 0 -3.048 5.334 -3.048 0.381 21
$PAD
Sh "3" O 2.49936 1.80086 0 0 900
Dr 1.19888 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 3.79984
$EndPAD
$PAD
Sh "1" O 2.49936 1.80086 0 0 900
Dr 1.19888 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3.50012 3.79984
$EndPAD
$PAD
Sh "5" O 2.49936 1.80086 0 0 900
Dr 1.19888 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3.50012 3.79984
$EndPAD
$PAD
Sh "4" O 2.49936 1.80086 0 0 900
Dr 1.19888 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1.80086 0.8001
$EndPAD
$PAD
Sh "2" O 2.49936 1.80086 0 0 900
Dr 1.19888 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1.80086 0.8001
$EndPAD
$SHAPE3D
Na "TO220-5_Pentawatt_Wings3d_RevA_24Oct2012/TO220-5_TO5D_Pentawatt_Vertical_Staggered_RevA_Faktor03937_24Oct2012.wrl"
Sc 0.3937 0.3937 0.3937
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE Pentawatt_Neutral_Staggered_Verical_TO220-5-T05D
$MODULE Pentawatt_Neutral_Straight_Vertical_TO220-5-T05A
Po 0 0 0 15 4DBC26C8 00000000 ~~
Li Pentawatt_Neutral_Straight_Vertical_TO220-5-T05A
Cd Pentawatt_Neutral_Straight_Vertical_TO220-5-T05A
Kw Pentawatt_Neutral_Straight_Vertical_TO220-5-T05A
Sc 0
AR 
Op 0 0 0
T0 0 -5.08 1.524 1.524 0 0.3048 N V 21 N "IC"
T1 1.27 7.62 1.524 1.524 0 0.3048 N V 21 N "Pentawatt_Neutral_Straight_Vertical_TO220-5-T05A"
DS 4.59994 -1.99898 5.30098 -1.99898 0.381 21
DS 2.60096 -1.99898 2.90068 -1.99898 0.381 21
DS 0.8001 -1.99898 1.00076 -1.99898 0.381 21
DS -0.89916 -1.99898 -0.8001 -1.99898 0.381 21
DS -2.79908 -1.99898 -2.70002 -1.99898 0.381 21
DS -5.30098 -1.99898 -4.59994 -1.99898 0.381 21
DS 1.50114 -2.99974 1.50114 -2.30124 0.381 21
DS -1.50114 -2.99974 -1.50114 -2.30124 0.381 21
DS -5.30098 1.80086 5.30098 1.80086 0.381 21
T2 -6.35 3.81 1.524 1.524 0 0.3048 N V 21 N "1"
DS 5.334 -1.905 5.334 1.778 0.381 21
DS -5.334 1.778 -5.334 -1.905 0.381 21
DS 5.334 -3.048 5.334 -1.905 0.381 21
DS -5.334 -1.905 -5.334 -3.048 0.381 21
DS 0 -3.048 -5.334 -3.048 0.381 21
DS 0 -3.048 5.334 -3.048 0.381 21
$PAD
Sh "3" O 2.49936 1.50114 0 0 900
Dr 1.09982 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 -0.59944
$EndPAD
$PAD
Sh "1" O 2.49936 1.50114 0 0 900
Dr 1.09982 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3.70078 -0.59944
$EndPAD
$PAD
Sh "5" O 2.49936 1.50114 0 0 900
Dr 1.09982 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3.70078 -0.59944
$EndPAD
$PAD
Sh "4" O 2.49936 1.50114 0 0 900
Dr 1.09982 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1.80086 -0.59944
$EndPAD
$PAD
Sh "2" O 2.49936 1.50114 0 0 900
Dr 1.09982 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1.80086 -0.59944
$EndPAD
$SHAPE3D
Na "TO220-5_Pentawatt_Wings3d_RevA_24Oct2012/TO220-5_TO5A_Pentawatt_Vertical_Inline_RevA_Faktor03937_24Oct2012.wrl"
Sc 0.3937 0.3937 0.3937
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE Pentawatt_Neutral_Straight_Vertical_TO220-5-T05A
$EndLIBRARY
