PCBNEW-LibModule-V1  So 21 Apr 2013 18:21:34 CEST
# encoding utf-8
Units mm
$INDEX
PowerMacro_PackageStyle-M234_NoHole_Housing_RevA_21Apr2013
PowerMacro_PackageStyle-M234_WithHole_Housing_RevA_21Apr2013
TO-50-3_LongPad-NoHole_Housing_RevA_21Apr2013
TO-50-3_LongPad-WithHole_Housing_RevA_21Apr2013
TO-50-3_ShortPad-NoHole_Housing_RevA_21Apr2013
TO-50-3_ShortPad-WithHole_Housing_RevA_21Apr2013
TO-50-4_LongPad-NoHole_Housing_RevA_21Apr2013
TO-50-4_LongPad-WithHole_Housing_RevA_21Apr2013
TO-50-4_ShortPad-NoHole_Housing_RevA_21Apr2013
TO-50-4_ShortPad-WithHole_Housing_RevA_21Apr2013
$EndINDEX
$MODULE PowerMacro_PackageStyle-M234_NoHole_Housing_RevA_21Apr2013
Po 0 0 0 15 51741026 00000000 ~~
Li PowerMacro_PackageStyle-M234_NoHole_Housing_RevA_21Apr2013
Kw TO-50-4, Power Macro, Package Style M234
Sc 0
AR 
Op 0 0 0
At SMD
T0 -0.15 -15.95 1.524 1.524 0 0.3048 N V 21 N "T"
T1 0.15 17.25 1.524 1.524 0 0.3048 N I 21 N "PowerMacro_PackageStyle-M234_NoHole_Housing_RevA_21Apr2013"
DC 0 0 2.6 0 0.381 21
$PAD
Sh "1" R 1.5 7 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -6.8
$EndPAD
$PAD
Sh "2" R 7 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.8 0
$EndPAD
$PAD
Sh "3" R 3.5 10 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 8.2
$EndPAD
$PAD
Sh "4" R 7 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6.8 0
$EndPAD
$EndMODULE PowerMacro_PackageStyle-M234_NoHole_Housing_RevA_21Apr2013
$MODULE PowerMacro_PackageStyle-M234_WithHole_Housing_RevA_21Apr2013
Po 0 0 0 15 51740FF3 00000000 ~~
Li PowerMacro_PackageStyle-M234_WithHole_Housing_RevA_21Apr2013
Kw TO-50-4, Power Macro, Package Style M234
Sc 0
AR 
Op 0 0 0
At SMD
T0 -0.15 -15.95 1.524 1.524 0 0.3048 N V 21 N "T"
T1 0.15 17.25 1.524 1.524 0 0.3048 N I 21 N "PowerMacro_PackageStyle-M234_WithHole_Housing_RevA_21Apr2013"
$PAD
Sh "1" R 1.5 7 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -6.8
$EndPAD
$PAD
Sh "2" R 7 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.8 0
$EndPAD
$PAD
Sh "3" R 3.5 10 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 8.2
$EndPAD
$PAD
Sh "" C 5.2 5.2 0 0 0
Dr 0 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "4" R 7 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6.8 0
$EndPAD
$EndMODULE PowerMacro_PackageStyle-M234_WithHole_Housing_RevA_21Apr2013
$MODULE TO-50-3_LongPad-NoHole_Housing_RevA_21Apr2013
Po 0 0 0 15 51740DDF 00000000 ~~
Li TO-50-3_LongPad-NoHole_Housing_RevA_21Apr2013
Kw TO-50-3, Macro T, Package Style M236
Sc 0
AR 
Op 0 0 0
At SMD
T0 -0.15 -15.95 1.524 1.524 0 0.3048 N V 21 N "T"
T1 0.15 17.25 1.524 1.524 0 0.3048 N I 21 N "TO-50-3_LongPad-NoHole_Housing_RevA_21Apr2013"
DC 0 0 2.6 0 0.381 21
$PAD
Sh "1" R 1.5 7 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -6.8
$EndPAD
$PAD
Sh "2" R 7 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.8 0
$EndPAD
$PAD
Sh "3" R 1.5 10 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 8.2
$EndPAD
$EndMODULE TO-50-3_LongPad-NoHole_Housing_RevA_21Apr2013
$MODULE TO-50-3_LongPad-WithHole_Housing_RevA_21Apr2013
Po 0 0 0 15 517409DC 00000000 ~~
Li TO-50-3_LongPad-WithHole_Housing_RevA_21Apr2013
Kw TO-50-3, Macro T, Package Style M236
Sc 0
AR 
Op 0 0 0
At SMD
T0 -0.15 -15.95 1.524 1.524 0 0.3048 N V 21 N "T"
T1 0.15 17.25 1.524 1.524 0 0.3048 N I 21 N "TO-50-3_LongPad-WithHole_Housing_RevA_21Apr2013"
$PAD
Sh "1" R 1.5 7 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -6.8
$EndPAD
$PAD
Sh "2" R 7 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.8 0
$EndPAD
$PAD
Sh "3" R 1.5 10 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 8.2
$EndPAD
$PAD
Sh "" C 5.2 5.2 0 0 0
Dr 0 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE TO-50-3_LongPad-WithHole_Housing_RevA_21Apr2013
$MODULE TO-50-3_ShortPad-NoHole_Housing_RevA_21Apr2013
Po 0 0 0 15 51740C2F 00000000 ~~
Li TO-50-3_ShortPad-NoHole_Housing_RevA_21Apr2013
Kw TO-50-3, Macro T, Package Style M236
Sc 0
AR 
Op 0 0 0
At SMD
T0 -0.15 -15.95 1.524 1.524 0 0.3048 N V 21 N "T"
T1 0.15 17.25 1.524 1.524 0 0.3048 N I 21 N "TO-50-3_ShortPad-NoHole_Housing_RevA_21Apr2013"
DC 0 0 2.7 0 0.381 21
$PAD
Sh "1" R 1.5 4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -5.2
$EndPAD
$PAD
Sh "2" R 4 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5.2 0
$EndPAD
$PAD
Sh "3" R 1.5 5.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 6
$EndPAD
$EndMODULE TO-50-3_ShortPad-NoHole_Housing_RevA_21Apr2013
$MODULE TO-50-3_ShortPad-WithHole_Housing_RevA_21Apr2013
Po 0 0 0 15 51740CCC 00000000 ~~
Li TO-50-3_ShortPad-WithHole_Housing_RevA_21Apr2013
Kw TO-50-3, Macro T, Package Style M236
Sc 0
AR 
Op 0 0 0
At SMD
T0 -0.15 -15.95 1.524 1.524 0 0.3048 N V 21 N "T"
T1 0.15 17.25 1.524 1.524 0 0.3048 N I 21 N "TO-50-3_ShortPad-WithHole_Housing_RevA_21Apr2013"
$PAD
Sh "1" R 1.5 4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -5.2
$EndPAD
$PAD
Sh "2" R 4 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5.2 0
$EndPAD
$PAD
Sh "3" R 1.5 5.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 6
$EndPAD
$PAD
Sh "" C 5.2 5.2 0 0 0
Dr 0 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE TO-50-3_ShortPad-WithHole_Housing_RevA_21Apr2013
$MODULE TO-50-4_LongPad-NoHole_Housing_RevA_21Apr2013
Po 0 0 0 15 51740E0C 00000000 ~~
Li TO-50-4_LongPad-NoHole_Housing_RevA_21Apr2013
Kw TO-50-4, Macro X, Package Style M238
Sc 0
AR 
Op 0 0 0
At SMD
T0 -0.15 -15.95 1.524 1.524 0 0.3048 N V 21 N "T"
T1 0.15 17.25 1.524 1.524 0 0.3048 N I 21 N "TO-50-4_LongPad-NoHole_Housing_RevA_21Apr2013"
DC 0 0 2.6 0 0.381 21
$PAD
Sh "1" R 1.5 7 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -6.8
$EndPAD
$PAD
Sh "2" R 7 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.8 0
$EndPAD
$PAD
Sh "3" R 1.5 10 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 8.2
$EndPAD
$PAD
Sh "4" R 7 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6.8 0
$EndPAD
$EndMODULE TO-50-4_LongPad-NoHole_Housing_RevA_21Apr2013
$MODULE TO-50-4_LongPad-WithHole_Housing_RevA_21Apr2013
Po 0 0 0 15 51740E5E 00000000 ~~
Li TO-50-4_LongPad-WithHole_Housing_RevA_21Apr2013
Kw TO-50-4, Macro X, Package Style M238
Sc 0
AR 
Op 0 0 0
At SMD
T0 -0.15 -15.95 1.524 1.524 0 0.3048 N V 21 N "T"
T1 0.15 17.25 1.524 1.524 0 0.3048 N I 21 N "TO-50-4_LongPad-WithHole_Housing_RevA_21Apr2013"
$PAD
Sh "1" R 1.5 7 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -6.8
$EndPAD
$PAD
Sh "2" R 7 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.8 0
$EndPAD
$PAD
Sh "3" R 1.5 10 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 8.2
$EndPAD
$PAD
Sh "" C 5.2 5.2 0 0 0
Dr 0 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "4" R 7 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6.8 0
$EndPAD
$EndMODULE TO-50-4_LongPad-WithHole_Housing_RevA_21Apr2013
$MODULE TO-50-4_ShortPad-NoHole_Housing_RevA_21Apr2013
Po 0 0 0 15 51740D94 00000000 ~~
Li TO-50-4_ShortPad-NoHole_Housing_RevA_21Apr2013
Kw TO-50-4, Macro X, Package Style M238
Sc 0
AR 
Op 0 0 0
At SMD
T0 -0.15 -15.95 1.524 1.524 0 0.3048 N V 21 N "T"
T1 0.15 17.25 1.524 1.524 0 0.3048 N I 21 N "TO-50-4_ShortPad-NoHole_Housing_RevA_21Apr2013"
DC 0 0 2.7 0 0.381 21
$PAD
Sh "1" R 1.5 4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -5.2
$EndPAD
$PAD
Sh "2" R 4 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5.2 0
$EndPAD
$PAD
Sh "3" R 1.5 5.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 6
$EndPAD
$PAD
Sh "4" R 4 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.2 0
$EndPAD
$EndMODULE TO-50-4_ShortPad-NoHole_Housing_RevA_21Apr2013
$MODULE TO-50-4_ShortPad-WithHole_Housing_RevA_21Apr2013
Po 0 0 0 15 51740D30 00000000 ~~
Li TO-50-4_ShortPad-WithHole_Housing_RevA_21Apr2013
Kw TO-50-4, Macro X, Package Style M238
Sc 0
AR 
Op 0 0 0
At SMD
T0 -0.15 -15.95 1.524 1.524 0 0.3048 N V 21 N "T"
T1 0.15 17.25 1.524 1.524 0 0.3048 N I 21 N "TO-50-3_ShortPad-WithHole_Housing_RevA_21Apr2013"
$PAD
Sh "1" R 1.5 4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -5.2
$EndPAD
$PAD
Sh "2" R 4 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5.2 0
$EndPAD
$PAD
Sh "3" R 1.5 5.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 6
$EndPAD
$PAD
Sh "" C 5.2 5.2 0 0 0
Dr 0 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "4" R 4 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.2 0
$EndPAD
$EndMODULE TO-50-4_ShortPad-WithHole_Housing_RevA_21Apr2013
$EndLIBRARY
